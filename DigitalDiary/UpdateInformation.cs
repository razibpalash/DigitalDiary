﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalDiary
{
    public partial class UpdateInformation : Form
    {
        string name, address, mobile, mail, fb;
        public UpdateInformation(string name,string address,string mobile,string mail,string fb)
        {
            InitializeComponent();
            this.name = name;
            this.address = address;
            this.mobile = mobile;
            this.mail = mail;
            this.fb = fb;
            textBox1.Text = name;
            textBox2.Text = address;
            textBox3.Text = mobile;
            textBox4.Text = mail;
            textBox5.Text = fb;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string myconnect = "datasource = localhost; port = 3306; username = root; password = root;";
            string myquery = "update  digitialdiary.aboutself set name = '" + this.textBox1.Text + "',address = '" + this.textBox2.Text + "',mobile = '" + this.textBox3.Text + "',mail = '" + this.textBox4.Text + "', fb = '" + this.textBox5.Text + "' where id=1; ";
            MySqlConnection myconnection = new MySqlConnection(myconnect);
            MySqlCommand mycommend = new MySqlCommand(myquery, myconnection);
            MySqlDataReader myReader;

            try
            {
                myconnection.Open();
                myReader = mycommend.ExecuteReader();
                MessageBox.Show("update your information");
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
            this.Close();
        }
    }
}
