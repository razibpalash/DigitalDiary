﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using System.IO;

namespace DigitalDiary
{
    public partial class ReadBook : Form
    {
        public ReadBook()
        {
            InitializeComponent();
            fillCombobox();            
        }

        private void fillCombobox()
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery = "select * from digitialdiary.book;";
            MySqlConnection myconnection = new MySqlConnection(connect);
            MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
            MySqlDataReader reader;
            try
            {
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    this.comboBox1.Items.Add(reader.GetString("bookname"));
                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery = "select * from digitialdiary.book where bookname = '" + this.comboBox1.Text + "';";
            MySqlConnection myconnection = new MySqlConnection(connect);
            MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
            MySqlDataReader reader;
            try
            {
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    byte[] pdf = (byte[])(reader["pdf"]);

                    if (pdf == null)
                    {
                        MessageBox.Show("Here is No pdf file");
                        pdfReader.src = null;
                    }
                    else
                    {
                        FileStream fStream = new FileStream(@"D:\\pdf test\\tests.pdf", FileMode.Create);
                        fStream.Write(pdf, 0, pdf.Length);
                        fStream.Close();

                        pdfReader.src = @"D:\\pdf test\\tests.pdf";
                    }

                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }
    }
}
