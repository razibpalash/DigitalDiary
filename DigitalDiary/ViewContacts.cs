﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalDiary
{
    public partial class ViewContacts : Form
    {
        public ViewContacts()
        {
            InitializeComponent();
            AutoComplete();
            showDatabase();
        }
        DataTable dataTableSet;
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DataView dv = new DataView(dataTableSet);
            dv.RowFilter = string.Format("code LIKE '%{0}%'", textBox1.Text);
            dataGridView1.DataSource = dv;
        }
        private void AutoComplete()
        {
            textBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection coll = new AutoCompleteStringCollection();

            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * FROM digitialdiary.contacts;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader reader;
            try
            {
                con.Open();
                reader = com.ExecuteReader();
                while (reader.Read())
                {
                    coll.Add(reader.GetString("name"));
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
            textBox1.AutoCompleteCustomSource = coll;

        }
        public void showDatabase()
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery = "select name,mobile from digitialdiary.contacts;";
            MySqlConnection myconnection = new MySqlConnection(connect);
            MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
            MySqlDataReader reader;
            try
            {
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    string name = reader.GetString("name");
                    string mobile = reader.GetString("mobile");
                    int n = dataGridView1.Rows.Add();
                    
                    dataGridView1.Rows[n].Cells[0].Value = name;
                    dataGridView1.Rows[n].Cells[1].Value = mobile;
                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }

        }
    }
}
