﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalDiary
{
    public partial class DeleteNote : Form
    {
        public DeleteNote()
        {
            InitializeComponent();
            fillCombo();
        }
        string sName;
        private void fillCombo()
        {
            string connect3 = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery3 = "select * from digitialdiary.note;";
            MySqlConnection myconnection3 = new MySqlConnection(connect3);
            MySqlCommand myCommand = new MySqlCommand(myQuery3, myconnection3);
            MySqlDataReader myReader3;

            try
            {
                myconnection3.Open();
                myReader3 = myCommand.ExecuteReader();
                while (myReader3.Read())
                {
                    sName = myReader3.GetString("filename");
                    comboBox1.Items.Add(sName);

                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(this.comboBox1.Text.ToString()))
            {
                string connect3 = "datasource = localhost; port = 3306; username = root; password = root";
                string myQuery3 = "select note,date from digitialdiary.note where filename='" + this.comboBox1.Text + "';";
                MySqlConnection myconnection3 = new MySqlConnection(connect3);
                MySqlCommand myCommand = new MySqlCommand(myQuery3, myconnection3);
                MySqlDataReader myReader3;

                try
                {
                    myconnection3.Open();
                    myReader3 = myCommand.ExecuteReader();
                    while (myReader3.Read())
                    {
                        string note = myReader3.GetString("note");
                        string date = myReader3.GetString("date");
                        richTextBox1.Text = note;
                        dateTimePicker1.Value = Convert.ToDateTime(date.ToString());

                    }
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.comboBox1.Text.ToString()))
            {
                string constring = "datasource=localhost;port=3306;username=root;password=root";
                string query = "delete from digitialdiary.note where filename='" + this.comboBox1.Text + "'";
                MySqlConnection conDatabase = new MySqlConnection(constring);
                MySqlCommand cmdDatabase = new MySqlCommand(query, conDatabase);
                MySqlDataReader myReader;
                try
                {
                    conDatabase.Open();
                    myReader = cmdDatabase.ExecuteReader();
                    MessageBox.Show("DELETE");
                    while (myReader.Read())
                    {
                    }
                    richTextBox1.Text = null;

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                this.Close();
            }
            else {
                MessageBox.Show("Please shearch first!!");
            }
        }
    }
}
