﻿namespace DigitalDiary
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aboutMyselfToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.noteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.remainderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.contactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.playToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.photoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.vediosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addVedioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playVedioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteVedioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.booksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteContactsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addBooksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.readBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteBookToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.eventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteEventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutMyselfToolStripMenuItem,
            this.noteToolStripMenuItem,
            this.remainderToolStripMenuItem,
            this.contactsToolStripMenuItem,
            this.photoToolStripMenuItem,
            this.vediosToolStripMenuItem,
            this.booksToolStripMenuItem,
            this.gamesToolStripMenuItem,
            this.eventToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(678, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aboutMyselfToolStripMenuItem
            // 
            this.aboutMyselfToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addInformationToolStripMenuItem,
            this.viewInformationToolStripMenuItem,
            this.updateInformationToolStripMenuItem,
            this.exitToolStripMenuItem8});
            this.aboutMyselfToolStripMenuItem.Name = "aboutMyselfToolStripMenuItem";
            this.aboutMyselfToolStripMenuItem.Size = new System.Drawing.Size(101, 21);
            this.aboutMyselfToolStripMenuItem.Text = "About Myself";
            // 
            // addInformationToolStripMenuItem
            // 
            this.addInformationToolStripMenuItem.Name = "addInformationToolStripMenuItem";
            this.addInformationToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.addInformationToolStripMenuItem.Text = "Add Information";
            this.addInformationToolStripMenuItem.Click += new System.EventHandler(this.addInformationToolStripMenuItem_Click);
            // 
            // viewInformationToolStripMenuItem
            // 
            this.viewInformationToolStripMenuItem.Name = "viewInformationToolStripMenuItem";
            this.viewInformationToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.viewInformationToolStripMenuItem.Text = "View Information";
            this.viewInformationToolStripMenuItem.Click += new System.EventHandler(this.viewInformationToolStripMenuItem_Click);
            // 
            // updateInformationToolStripMenuItem
            // 
            this.updateInformationToolStripMenuItem.Name = "updateInformationToolStripMenuItem";
            this.updateInformationToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.updateInformationToolStripMenuItem.Text = "Update Information";
            this.updateInformationToolStripMenuItem.Click += new System.EventHandler(this.updateInformationToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem8
            // 
            this.exitToolStripMenuItem8.Name = "exitToolStripMenuItem8";
            this.exitToolStripMenuItem8.Size = new System.Drawing.Size(198, 22);
            this.exitToolStripMenuItem8.Text = "Exit";
            this.exitToolStripMenuItem8.Click += new System.EventHandler(this.exitToolStripMenuItem8_Click);
            // 
            // noteToolStripMenuItem
            // 
            this.noteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.deleteToolStripMenuItem1,
            this.exitToolStripMenuItem7});
            this.noteToolStripMenuItem.Name = "noteToolStripMenuItem";
            this.noteToolStripMenuItem.Size = new System.Drawing.Size(49, 21);
            this.noteToolStripMenuItem.Text = "Note";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.addToolStripMenuItem.Text = "Add Note";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.viewToolStripMenuItem.Text = "View Note";
            this.viewToolStripMenuItem.Click += new System.EventHandler(this.viewToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteToolStripMenuItem.Text = "Edit Note";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.deleteToolStripMenuItem1.Text = "Delete Note";
            this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
            // 
            // exitToolStripMenuItem7
            // 
            this.exitToolStripMenuItem7.Name = "exitToolStripMenuItem7";
            this.exitToolStripMenuItem7.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem7.Text = "Exit";
            this.exitToolStripMenuItem7.Click += new System.EventHandler(this.exitToolStripMenuItem7_Click);
            // 
            // remainderToolStripMenuItem
            // 
            this.remainderToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem1,
            this.viewToolStripMenuItem1,
            this.editToolStripMenuItem,
            this.exitToolStripMenuItem6});
            this.remainderToolStripMenuItem.Name = "remainderToolStripMenuItem";
            this.remainderToolStripMenuItem.Size = new System.Drawing.Size(87, 21);
            this.remainderToolStripMenuItem.Text = "Remainder";
            // 
            // addToolStripMenuItem1
            // 
            this.addToolStripMenuItem1.Name = "addToolStripMenuItem1";
            this.addToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.addToolStripMenuItem1.Text = "Add Remainder";
            this.addToolStripMenuItem1.Click += new System.EventHandler(this.addToolStripMenuItem1_Click);
            // 
            // viewToolStripMenuItem1
            // 
            this.viewToolStripMenuItem1.Name = "viewToolStripMenuItem1";
            this.viewToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.viewToolStripMenuItem1.Text = "View Remainder";
            this.viewToolStripMenuItem1.Click += new System.EventHandler(this.viewToolStripMenuItem1_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.editToolStripMenuItem.Text = "Edit Remainder";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem6
            // 
            this.exitToolStripMenuItem6.Name = "exitToolStripMenuItem6";
            this.exitToolStripMenuItem6.Size = new System.Drawing.Size(177, 22);
            this.exitToolStripMenuItem6.Text = "Exit";
            this.exitToolStripMenuItem6.Click += new System.EventHandler(this.exitToolStripMenuItem6_Click);
            // 
            // contactsToolStripMenuItem
            // 
            this.contactsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem2,
            this.playToolStripMenuItem,
            this.deleteToolStripMenuItem2,
            this.exitToolStripMenuItem5});
            this.contactsToolStripMenuItem.Name = "contactsToolStripMenuItem";
            this.contactsToolStripMenuItem.Size = new System.Drawing.Size(55, 21);
            this.contactsToolStripMenuItem.Text = "Music";
            // 
            // addToolStripMenuItem2
            // 
            this.addToolStripMenuItem2.Name = "addToolStripMenuItem2";
            this.addToolStripMenuItem2.Size = new System.Drawing.Size(155, 22);
            this.addToolStripMenuItem2.Text = "Add Music";
            // 
            // playToolStripMenuItem
            // 
            this.playToolStripMenuItem.Name = "playToolStripMenuItem";
            this.playToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.playToolStripMenuItem.Text = "Play Music";
            this.playToolStripMenuItem.Click += new System.EventHandler(this.playToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem2
            // 
            this.deleteToolStripMenuItem2.Name = "deleteToolStripMenuItem2";
            this.deleteToolStripMenuItem2.Size = new System.Drawing.Size(155, 22);
            this.deleteToolStripMenuItem2.Text = "Delete Music";
            // 
            // exitToolStripMenuItem5
            // 
            this.exitToolStripMenuItem5.Name = "exitToolStripMenuItem5";
            this.exitToolStripMenuItem5.Size = new System.Drawing.Size(155, 22);
            this.exitToolStripMenuItem5.Text = "Exit";
            // 
            // photoToolStripMenuItem
            // 
            this.photoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem3,
            this.viewToolStripMenuItem2,
            this.deleteToolStripMenuItem3,
            this.exitToolStripMenuItem4});
            this.photoToolStripMenuItem.Name = "photoToolStripMenuItem";
            this.photoToolStripMenuItem.Size = new System.Drawing.Size(61, 21);
            this.photoToolStripMenuItem.Text = "Photos";
            // 
            // addToolStripMenuItem3
            // 
            this.addToolStripMenuItem3.Name = "addToolStripMenuItem3";
            this.addToolStripMenuItem3.Size = new System.Drawing.Size(155, 22);
            this.addToolStripMenuItem3.Text = "Add Photo";
            this.addToolStripMenuItem3.Click += new System.EventHandler(this.addToolStripMenuItem3_Click);
            // 
            // viewToolStripMenuItem2
            // 
            this.viewToolStripMenuItem2.Name = "viewToolStripMenuItem2";
            this.viewToolStripMenuItem2.Size = new System.Drawing.Size(155, 22);
            this.viewToolStripMenuItem2.Text = "View Photo";
            this.viewToolStripMenuItem2.Click += new System.EventHandler(this.viewToolStripMenuItem2_Click);
            // 
            // deleteToolStripMenuItem3
            // 
            this.deleteToolStripMenuItem3.Name = "deleteToolStripMenuItem3";
            this.deleteToolStripMenuItem3.Size = new System.Drawing.Size(155, 22);
            this.deleteToolStripMenuItem3.Text = "Delete Photo";
            this.deleteToolStripMenuItem3.Click += new System.EventHandler(this.deleteToolStripMenuItem3_Click);
            // 
            // exitToolStripMenuItem4
            // 
            this.exitToolStripMenuItem4.Name = "exitToolStripMenuItem4";
            this.exitToolStripMenuItem4.Size = new System.Drawing.Size(155, 22);
            this.exitToolStripMenuItem4.Text = "Exit";
            this.exitToolStripMenuItem4.Click += new System.EventHandler(this.exitToolStripMenuItem4_Click);
            // 
            // vediosToolStripMenuItem
            // 
            this.vediosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addVedioToolStripMenuItem,
            this.playVedioToolStripMenuItem,
            this.deleteVedioToolStripMenuItem,
            this.exitToolStripMenuItem3});
            this.vediosToolStripMenuItem.Name = "vediosToolStripMenuItem";
            this.vediosToolStripMenuItem.Size = new System.Drawing.Size(55, 21);
            this.vediosToolStripMenuItem.Text = "Vedio";
            // 
            // addVedioToolStripMenuItem
            // 
            this.addVedioToolStripMenuItem.Name = "addVedioToolStripMenuItem";
            this.addVedioToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.addVedioToolStripMenuItem.Text = "Add Vedio";
            // 
            // playVedioToolStripMenuItem
            // 
            this.playVedioToolStripMenuItem.Name = "playVedioToolStripMenuItem";
            this.playVedioToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.playVedioToolStripMenuItem.Text = "Play Vedio";
            // 
            // deleteVedioToolStripMenuItem
            // 
            this.deleteVedioToolStripMenuItem.Name = "deleteVedioToolStripMenuItem";
            this.deleteVedioToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.deleteVedioToolStripMenuItem.Text = "Delete Vedio";
            // 
            // exitToolStripMenuItem3
            // 
            this.exitToolStripMenuItem3.Name = "exitToolStripMenuItem3";
            this.exitToolStripMenuItem3.Size = new System.Drawing.Size(155, 22);
            this.exitToolStripMenuItem3.Text = "Exit";
            // 
            // booksToolStripMenuItem
            // 
            this.booksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addContactsToolStripMenuItem,
            this.viewContactsToolStripMenuItem,
            this.deleteContactsToolStripMenuItem,
            this.exitToolStripMenuItem2});
            this.booksToolStripMenuItem.Name = "booksToolStripMenuItem";
            this.booksToolStripMenuItem.Size = new System.Drawing.Size(73, 21);
            this.booksToolStripMenuItem.Text = "Contacts";
            // 
            // addContactsToolStripMenuItem
            // 
            this.addContactsToolStripMenuItem.Name = "addContactsToolStripMenuItem";
            this.addContactsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.addContactsToolStripMenuItem.Text = "Add Contacts";
            this.addContactsToolStripMenuItem.Click += new System.EventHandler(this.addContactsToolStripMenuItem_Click);
            // 
            // viewContactsToolStripMenuItem
            // 
            this.viewContactsToolStripMenuItem.Name = "viewContactsToolStripMenuItem";
            this.viewContactsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.viewContactsToolStripMenuItem.Text = "View Contacts";
            this.viewContactsToolStripMenuItem.Click += new System.EventHandler(this.viewContactsToolStripMenuItem_Click);
            // 
            // deleteContactsToolStripMenuItem
            // 
            this.deleteContactsToolStripMenuItem.Name = "deleteContactsToolStripMenuItem";
            this.deleteContactsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.deleteContactsToolStripMenuItem.Text = "Delete Contacts";
            this.deleteContactsToolStripMenuItem.Click += new System.EventHandler(this.deleteContactsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem2
            // 
            this.exitToolStripMenuItem2.Name = "exitToolStripMenuItem2";
            this.exitToolStripMenuItem2.Size = new System.Drawing.Size(173, 22);
            this.exitToolStripMenuItem2.Text = "Exit";
            this.exitToolStripMenuItem2.Click += new System.EventHandler(this.exitToolStripMenuItem2_Click);
            // 
            // gamesToolStripMenuItem
            // 
            this.gamesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addBooksToolStripMenuItem,
            this.readBookToolStripMenuItem,
            this.deleteBookToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.gamesToolStripMenuItem.Name = "gamesToolStripMenuItem";
            this.gamesToolStripMenuItem.Size = new System.Drawing.Size(55, 21);
            this.gamesToolStripMenuItem.Text = "Books";
            // 
            // addBooksToolStripMenuItem
            // 
            this.addBooksToolStripMenuItem.Name = "addBooksToolStripMenuItem";
            this.addBooksToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.addBooksToolStripMenuItem.Text = "Add Books";
            this.addBooksToolStripMenuItem.Click += new System.EventHandler(this.addBooksToolStripMenuItem_Click);
            // 
            // readBookToolStripMenuItem
            // 
            this.readBookToolStripMenuItem.Name = "readBookToolStripMenuItem";
            this.readBookToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.readBookToolStripMenuItem.Text = "Read Book";
            this.readBookToolStripMenuItem.Click += new System.EventHandler(this.readBookToolStripMenuItem_Click);
            // 
            // deleteBookToolStripMenuItem
            // 
            this.deleteBookToolStripMenuItem.Name = "deleteBookToolStripMenuItem";
            this.deleteBookToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.deleteBookToolStripMenuItem.Text = "Delete Book";
            this.deleteBookToolStripMenuItem.Click += new System.EventHandler(this.deleteBookToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(149, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            // 
            // eventToolStripMenuItem
            // 
            this.eventToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addEventToolStripMenuItem,
            this.viewEventToolStripMenuItem,
            this.deleteEventToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.eventToolStripMenuItem.Name = "eventToolStripMenuItem";
            this.eventToolStripMenuItem.Size = new System.Drawing.Size(54, 21);
            this.eventToolStripMenuItem.Text = "Event";
            // 
            // addEventToolStripMenuItem
            // 
            this.addEventToolStripMenuItem.Name = "addEventToolStripMenuItem";
            this.addEventToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.addEventToolStripMenuItem.Text = "Add Event";
            // 
            // viewEventToolStripMenuItem
            // 
            this.viewEventToolStripMenuItem.Name = "viewEventToolStripMenuItem";
            this.viewEventToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.viewEventToolStripMenuItem.Text = "View Event";
            // 
            // deleteEventToolStripMenuItem
            // 
            this.deleteEventToolStripMenuItem.Name = "deleteEventToolStripMenuItem";
            this.deleteEventToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.deleteEventToolStripMenuItem.Text = "Delete Event";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.BackgroundImage = global::DigitalDiary.Properties.Resources.fdtfgtyh;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(678, 330);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutMyselfToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem remainderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem playToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem photoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem vediosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addVedioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem playVedioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteVedioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem booksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gamesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eventToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem addContactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewContactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteContactsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem addBooksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem readBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteBookToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addEventToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewEventToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteEventToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}