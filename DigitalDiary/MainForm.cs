﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace DigitalDiary
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void playToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        string reminderDate;
        private void viewToolStripMenuItem1_Click(object sender, EventArgs e)
        {


            string date = System.DateTime.Now.Year.ToString() + "-" + System.DateTime.Now.Month.ToString() + "-" + System.DateTime.Now.Day.ToString();
            string myconnect = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection myconnecttion = new MySqlConnection(myconnect);
            string command = "select reminder from digitialdiary.reminder where date='"+date.ToString()+"' ;";
            MySqlCommand mycommand = new MySqlCommand(command, myconnecttion);
            try
            {
                MySqlDataReader myReader;
                myconnecttion.Open();
                myReader = mycommand.ExecuteReader();

                while (myReader.Read())
                {
                    reminderDate = myReader.GetString("reminder");
                    

                }
                if (reminderDate != null)
                {
                    MessageBox.Show(reminderDate);
                }
                else 
                {
                    MessageBox.Show("you don't have any reminder this date!!");
                }

                myconnecttion.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
            
        }

        private void addInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddInformation addInfo = new AddInformation();
            addInfo.ShowDialog();
        }

        string photopath1, photoname;
        private void addToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            try
            {
                //FOR OPEN A PHOTO INTO DEVICE
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "JEG FILE(*.jpg)|*.jpg|PNG FILE(*.png)|*.png|All FILE(*.*)|*.*";
                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    photopath1 = openFileDialog.FileName.ToString();
                    photoname = openFileDialog.SafeFileName.ToString();
                }

                string connect = "datasource = localhost; port = 3306; username = root; password = root";
                string myQuery = "select * from digitialdiary.photo where name='"+photoname.ToString()+"';";
                MySqlConnection myconnection = new MySqlConnection(connect);
                MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
                MySqlDataReader reader;
            
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                int counter = 0;
                while (reader.Read())
                {
                    counter++;
                }
                if (counter == 0)
                {
                    addPhoto();
                }
                else
                {
                    MessageBox.Show("Please change your file name!!");
                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show("please select your photo");
            }

        }

        private void addPhoto()
        {
            try
            {
                
                //INSERT PHOTO INTO DATABASE
                byte[] imageBt = null;
                FileStream fstream = new FileStream(photopath1, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fstream);
                imageBt = br.ReadBytes((int)fstream.Length);

                string conn = "datasource = localhost; port = 3306; username = root; password = root;";
                string query = "insert into digitialdiary.photo (photo, name) values(@IMG, '" + photoname + "')";
                MySqlConnection connection = new MySqlConnection(conn);
                MySqlCommand commend = new MySqlCommand(query, connection);
                MySqlDataReader reader;

                connection.Open();
                commend.Parameters.Add(new MySqlParameter("@IMG", imageBt));
                reader = commend.ExecuteReader();
                MessageBox.Show("saved");
                connection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void viewInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewInformation viewInfo = new ViewInformation();
            viewInfo.ShowDialog();
        }

        string name, address, mobile, mail, fb;
        private void updateInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getValue();
            UpdateInformation updateInfo=new UpdateInformation(name,address,mobile,mail,fb);
            updateInfo.ShowDialog();
        }

        private void getValue()
        {
            string myconnect = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection myconnecttion = new MySqlConnection(myconnect);
            string command = "select * from digitialdiary.aboutself where id = 1;";
            MySqlCommand mycommand = new MySqlCommand(command, myconnecttion);

            try
            {
                MySqlDataReader myReader;
                myconnecttion.Open();
                myReader = mycommand.ExecuteReader();
                while (myReader.Read())
                {
                    name = myReader.GetString("name");
                    address = myReader.GetString("address");
                    mobile = myReader.GetString("mobile");
                    mail = myReader.GetString("mail");
                    fb = myReader.GetString("fb");

                }
                myconnecttion.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void exitToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void viewToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ViewPhoto viewPhoto = new ViewPhoto();
            viewPhoto.ShowDialog();
        }

        private void addContactsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddContacts addcon = new AddContacts();
            addcon.ShowDialog();
        }

        private void viewContactsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewContacts viewcon = new ViewContacts();
            viewcon.ShowDialog();
        }

        private void deleteContactsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteContacts deletecon = new DeleteContacts();
            deletecon.ShowDialog();
        }

        string path, filename;
        private void addBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "PDF FILE(*.PDF)|*.pdf|All FILE(*.*)|*.*";
                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    path = openFileDialog.FileName.ToString();
                    filename = openFileDialog.SafeFileName.ToString();
                }
                string connect = "datasource = localhost; port = 3306; username = root; password = root";
                string myQuery = "select * from digitialdiary.book where bookname='" + filename.ToString() + "';";
                MySqlConnection myconnection = new MySqlConnection(connect);
                MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
                MySqlDataReader reader;
            
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                int counter = 0;
                while (reader.Read())
                {
                    counter++;
                }
                if (counter == 0)
                {
                    insertBook();
                }
                else
                {
                    MessageBox.Show("Please change your file name!!");
                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                //MessageBox.Show(x.Message);
                MessageBox.Show("please select your pdf files");
            }
        }

        private void insertBook()
        {
            try
            {
                byte[] pdfData = null;
                FileStream fstream = new FileStream(this.path, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fstream);
                pdfData = br.ReadBytes((int)fstream.Length);

                string conn = "datasource = localhost; port = 3306; username = root; password = root;";
                string query = "insert into digitialdiary.book (bookname,pdf) values('" + this.filename + "', @data)";
                MySqlConnection connection = new MySqlConnection(conn);
                MySqlCommand commend = new MySqlCommand(query, connection);
                MySqlDataReader reader;
            
                connection.Open();
                commend.Parameters.Add(new MySqlParameter("@data", pdfData));
                reader = commend.ExecuteReader();
                MessageBox.Show("saved");
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void readBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReadBook readbook = new ReadBook();
            readbook.ShowDialog();
        }

        private void deleteBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteBooks deletebook = new DeleteBooks();
            deletebook.ShowDialog();
        }

        private void exitToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void deleteToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            DeletePhoto deletephoto = new DeletePhoto();
            deletephoto.ShowDialog();
        }

        private void exitToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNote addNote = new AddNote();
            addNote.ShowDialog();
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewNote viewNote = new ViewNote();
            viewNote.ShowDialog();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditNote editNote = new EditNote();
            editNote.ShowDialog();
        }

        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DeleteNote deleteNote = new DeleteNote();
            deleteNote.ShowDialog();
        }

        private void exitToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void addToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddReminder addreminder = new AddReminder();
            addreminder.ShowDialog();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditReminder editReminder = new EditReminder();
            editReminder.ShowDialog();
        }

        private void exitToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
