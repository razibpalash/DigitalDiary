﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalDiary
{
    public partial class AddInformation : Form
    {
        public AddInformation()
        {
            InitializeComponent();
        }

        private void AddInformation_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.textBox1.Text.ToString()) && !string.IsNullOrEmpty(this.textBox2.Text.ToString()) && !string.IsNullOrEmpty(this.textBox3.Text.ToString()) && !string.IsNullOrEmpty(this.textBox4.Text.ToString()) && !string.IsNullOrEmpty(this.textBox5.Text.ToString()))
            {
                string myconnect = "datasource = localhost; port = 3306; username = root; password = root;";
                MySqlConnection myconnecttion = new MySqlConnection(myconnect);
                string command = "select * from digitialdiary.aboutself ;";
                MySqlCommand mycommand = new MySqlCommand(command, myconnecttion);

                try
                {
                    MySqlDataReader myReader;
                    myconnecttion.Open();
                    myReader = mycommand.ExecuteReader();
                    int counter = 0;
                    while (myReader.Read())
                    {
                        counter++;
                        updateInformation();
                        this.Close();
                    }
                    if (counter == 0)
                    {
                        insertInformation();
                        this.Close();
                    }
                }
                catch (Exception x)
                {
                    MessageBox.Show(x.Message);
                }
            }
            else 
            {
                MessageBox.Show("Please add all information!!");
            }
        }

        private void insertInformation()
        {
            string myconnect = "datasource = localhost; port = 3306; username = root; password = root;";
            string myQuery = "insert into digitialdiary.aboutself (name,address, mobile,mail, fb) values('" + this.textBox1.Text + "','" + this.textBox2.Text + "','" + this.textBox3.Text + "','" + this.textBox4.Text + "','" + this.textBox5.Text + "') ;";
            MySqlConnection myconnection = new MySqlConnection(myconnect);
            MySqlCommand mycommand = new MySqlCommand(myQuery, myconnection);
            MySqlDataReader myreader2;

            try
            {
                myconnection.Open();
                myreader2 = mycommand.ExecuteReader();
                MessageBox.Show("save");
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void updateInformation()
        {
            string myconnect = "datasource = localhost; port = 3306; username = root; password = root;";
            string myquery = "update  digitialdiary.aboutself set name = '" + this.textBox1.Text + "',address = '" + this.textBox2.Text + "',mobile = '" + this.textBox3.Text + "',mail = '" + this.textBox4.Text + "', fb = '" + this.textBox5.Text + "' where id=1; ";
            MySqlConnection myconnection = new MySqlConnection(myconnect);
            MySqlCommand mycommend = new MySqlCommand(myquery, myconnection);
            MySqlDataReader myReader;

            try
            {
                myconnection.Open();
                myReader = mycommend.ExecuteReader();
                MessageBox.Show("update your information");
                myconnection.Close();              
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }
    }
}
