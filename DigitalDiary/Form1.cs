﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalDiary
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            password_txt.PasswordChar = '*';
        }
        //FOR LOG IN 
        string name, pass;
        private void btnLogIn_Click(object sender, EventArgs e)
        {
            name = userName_txt.Text;
            pass = password_txt.Text;

            if (name == "test" && pass == "test")
            {
                MainForm addForm = new MainForm();
                addForm.ShowDialog();
                this.Close();
            }
        }

        private void password_txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            MainForm addForm = new MainForm();
            if (e.KeyChar == (char)Keys.Enter)
            {
                //MessageBox.Show("%");
                if (userName_txt.Text == "samrat" && password_txt.Text == "samrat")
                {
                    //MessageBox.Show("%");
                    addForm.ShowDialog();
                    this.Close();
                }
            }
        }
    }
}
