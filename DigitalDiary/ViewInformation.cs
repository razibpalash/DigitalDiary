﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalDiary
{
    public partial class ViewInformation : Form
    {
        public ViewInformation()
        {
            InitializeComponent();

            viewInformation();
        }

        private void viewInformation()
        {
            string myconnect = "datasource = localhost; port = 3306; username = root; password = root;";
            MySqlConnection myconnecttion = new MySqlConnection(myconnect);
            string command = "select * from digitialdiary.aboutself where id = 1;";
            MySqlCommand mycommand = new MySqlCommand(command, myconnecttion);

            try
            {
                MySqlDataReader myReader;
                myconnecttion.Open();
                myReader = mycommand.ExecuteReader();
                while (myReader.Read())
                {
                    string name = myReader.GetString("name");
                    string address = myReader.GetString("address");
                    string mobile = myReader.GetString("mobile");
                    string mail = myReader.GetString("mail");
                    string fb = myReader.GetString("fb");
                    label7.Text = name;
                    label6.Text = address;
                    label8.Text = mobile;
                    label9.Text = mail;
                    label10.Text = fb;
                }
                myconnecttion.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }
    }
}
