﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalDiary
{
    public partial class DeletePhoto : Form
    {
        public DeletePhoto()
        {
            InitializeComponent();
            fillCombobox();
        }

        private void fillCombobox()
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery = "select * from digitialdiary.photo;";
            MySqlConnection myconnection = new MySqlConnection(connect);
            MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
            MySqlDataReader reader;
            try
            {
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    this.comboBox1.Items.Add(reader.GetString("name"));
                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery = "select * from digitialdiary.photo where name = '" + this.comboBox1.Text + "';";
            MySqlConnection myconnection = new MySqlConnection(connect);
            MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
            MySqlDataReader reader;
            try
            {
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    byte[] imgg = (byte[])(reader["photo"]);

                    if (imgg == null)
                    {
                        MessageBox.Show("Here is No Image");
                        pictureBox1.Image = null;
                    }
                    else
                    {
                        MemoryStream mStream = new MemoryStream(imgg);
                        pictureBox1.Image = Image.FromStream(mStream);
                    }

                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string constring = "datasource=localhost;port=3306;username=root;password=root";
            string query = "delete from digitialdiary.photo where name ='" + comboBox1.Text + "';";
            MySqlConnection conDatabase = new MySqlConnection(constring);
            MySqlCommand cmdDatabase = new MySqlCommand(query, conDatabase);
            MySqlDataReader myReader;
            try
            {
                conDatabase.Open();
                myReader = cmdDatabase.ExecuteReader();
                MessageBox.Show("DELETE");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
