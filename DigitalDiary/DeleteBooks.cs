﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using MySql.Data.MySqlClient;

namespace DigitalDiary
{
    public partial class DeleteBooks : Form
    {
        public DeleteBooks()
        {
            InitializeComponent();
            fillCombobox();
        }

        private void fillCombobox()
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery = "select * from digitialdiary.book;";
            MySqlConnection myconnection = new MySqlConnection(connect);
            MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
            MySqlDataReader reader;
            try
            {
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    this.comboBox1.Items.Add(reader.GetString("bookname"));
                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery = "select * from digitialdiary.book where bookname = '" + this.comboBox1.Text + "';";
            MySqlConnection myconnection = new MySqlConnection(connect);
            MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);
            MySqlDataReader reader;
            try
            {
                myconnection.Open();
                reader = myCommand.ExecuteReader();
                while (reader.Read())
                {
                    byte[] pdf = (byte[])(reader["pdf"]);

                    if (pdf == null)
                    {
                        MessageBox.Show("Here is No Image");
                        axAcroPDF1.src = null;
                    }
                    else
                    {
                        FileStream fStream = new FileStream(@"D:\\pdf test\\tests.pdf", FileMode.Create);
                        fStream.Write(pdf, 0, pdf.Length);
                        fStream.Close();

                        axAcroPDF1.src = @"D:\\pdf test\\tests.pdf";
                    }

                }
                myconnection.Close();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string constring = "datasource=localhost;port=3306;username=root;password=root";
            string query = "delete from digitialdiary.book where bookname ='" + comboBox1.Text + "';";
            MySqlConnection conDatabase = new MySqlConnection(constring);
            MySqlCommand cmdDatabase = new MySqlCommand(query, conDatabase);
            MySqlDataReader myReader;
            try
            {
                conDatabase.Open();
                myReader = cmdDatabase.ExecuteReader();
                MessageBox.Show("DELETE");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void axAcroPDF1_OnError(object sender, EventArgs e)
        {

        }
    }
}
