﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigitalDiary
{
    public partial class DeleteContacts : Form
    {
        public DeleteContacts()
        {
            InitializeComponent();
            AutoComplete();
            showDatabase();
        }
        DataTable dataTableSet;
        string name;
        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            name = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            DataView dv = new DataView(dataTableSet);
            dv.RowFilter = string.Format("name LIKE '%{0}%'", textBox1.Text);
            dataGridView1.DataSource = dv;
        }
        private void AutoComplete()
        {
            textBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;
            AutoCompleteStringCollection coll = new AutoCompleteStringCollection();

            string connection = "datasource = localhost; port = 3306; username = root; password = root;";
            string query = "SELECT * FROM digitialdiary.contacts;";
            MySqlConnection con = new MySqlConnection(connection);
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader reader;
            try
            {
                con.Open();
                reader = com.ExecuteReader();
                while (reader.Read())
                {
                    coll.Add(reader.GetString("name"));
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
            textBox1.AutoCompleteCustomSource = coll;

        }
        public void showDatabase()
        {
            //dataGridView1.Rows.Clear();
            string connect = "datasource = localhost; port = 3306; username = root; password = root";
            string myQuery = "select name,mobile from digitialdiary.contacts;";
            MySqlConnection myconnection = new MySqlConnection(connect);
            MySqlCommand myCommand = new MySqlCommand(myQuery, myconnection);

            try
            {
                MySqlDataAdapter myDataAdapter = new MySqlDataAdapter();
                myDataAdapter.SelectCommand = myCommand;
                dataTableSet = new DataTable();
                myDataAdapter.Fill(dataTableSet);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dataTableSet;
                dataGridView1.DataSource = bSource;
                myDataAdapter.Update(dataTableSet);

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count != 0)
            {
                try
                {

                    string constring = "datasource=localhost;port=3306;username=root;password=root";
                    string query = "delete from digitialdiary.contacts where name='" + name.ToString() + "'";
                    MySqlConnection conDatabase = new MySqlConnection(constring);
                    MySqlCommand cmdDatabase = new MySqlCommand(query, conDatabase);
                    MySqlDataReader myReader;
                    try
                    {
                        conDatabase.Open();
                        myReader = cmdDatabase.ExecuteReader();
                        MessageBox.Show("DELETE");
                        while (myReader.Read())
                        {
                        }
                        // dataGridView1.Rows.Clear();
                        showDatabase();
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
